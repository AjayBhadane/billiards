﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallBehaviourScript : MonoBehaviour
{
    void Awake()
    {
        Invoke("HitBall", 1f);
    }

    private void HitBall()
    {
        gameObject.GetComponent<Rigidbody>().AddForce(200, 0, 0);
    }
}
